# AndroidEngineeringDemo :rocket:

A simple sample app to showcase integration of GitLab CI + Fastlane.

### GitLab CI Basics :notebook:

- Pipelines - Gitlab pipeline is series of jobs that follows a certain order.
- Jobs - These are small units that runs in a pipeline. Usually consists of commands to build, test, or execute.
- Stages - You can group jobs into their different stages. Jobs in the same stage runs in parallel.
- Triggers - This is a configuration that tells CI when to run our pipelines.
